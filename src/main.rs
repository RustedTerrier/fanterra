mod game;
mod setup;

use std::{env, io};

fn main() {
    // if 2 < 3 {
    // extern crate rodio;
    // }
    start_screen();
}

fn start_screen() {
    println!("{}", 3 ^ 2);
    println!("AMMMMMMMMMMA     AMMA      AMA     AMA  AMMMMMMMMMMMMMA  AMMMMMMMMMMA  AMMMMMMA.    AMMMMMMA.        AMMA\n\
              MMMMMMMMMMMV    AMVVMA     MMMA    MMM  VMMMMMMMMMMMMMV  MMMMMMMMMMMV  MMMMMMMMMA   MMMMMMMMMA      AMVVMA\n\
              MMM            AMV  VMA    MMMMA   MMM        MMM        MMM           MMM'   `VMA  MMM'   `VMA    AMV  VMA\n\
              MMM           AMV    VMA   MMMVMA  MMM        MMM        MMM           MMM     ;MM  MMM     ;MM   AMV    VMA\n\
              MMMMMMMMA    AMV      VMA  MMM VMA MMM        MMM        MMMMMMMMMA    MMM.   .AMV  MMM.   .AMV  AMV      VMA\n\
              MMMMMMMMV    MMMMMMMMMMMM  MMM  VMAMMM        MMM        MMMMMMMMMV    MMMMMMMMMV   MMMMMMMMMV   MMMMMMMMMMMM\n\
              MMM          MMMMMMMMMMMM  MMM   VMMMM        MMM        MMM           MMMMMMMMA'   MMMMMMMMA'   MMMMMMMMMMMM\n\
              MMM          MMM      MMM  MMM    VMMM        MMM        MMMMMMMMMMMA  MMM    VMA   MMM    VMA   MMM      MMM\n\
              VMV          VMV      VMV  VMV     VMV        VMV        VMMMMMMMMMMV  VMV     VMA  VMV     VMA  VMV      VMV\n\
              ");
    println!("Do you want to do? 1) Create a new game, 2) Play an existing game, or 3) Quit?");

    let mut choice = String::new();
    io::stdin().read_line(&mut choice).expect("Failed to read line");
    choice = choice.replace("\n", "");

    if choice == *"1" {
        // If you want to create a new world, create a new world.
        let world_name = setup::create_world(env::var("HOME").unwrap());
        play_game(world_name);
    }
    if choice == *"2" {
        // If you want to play an existing game, do that.
        let worlds_string: String = setup::read_worlds(env::var("HOME").unwrap()).into_iter().collect();
        println!("Choose from each world, with a corresponding number: \n\r{}", &worlds_string);

        let mut world = String::new();
        io::stdin().read_line(&mut world).expect("Something went wrong reading your input.");

        let worldnum = world.replace('\n', "").parse::<u32>().unwrap() - 1;
        let worldsplit: Vec<&str> = worlds_string.split('\n').collect();
        world = worldsplit[worldnum as usize].to_string().replace("  |", "");
        world = format_world(world);

        play_game(world);
    }
    if choice == *"3" {
        // If you want to quit, quit.
        println!("Quiting...");
    }
}

fn play_game(world: String) {
    let game = setup::setup_game(world, env::var("HOME").unwrap());

    // pa1, is supposed to mean path1, I'm just lazy at typing.
    let mut game_data = game::start_game(game.seed, game.pa1, game.pa2, game.pa3);
    loop {
        game_data.area += 1;
        match game_data.path {
            | 1 => {
                let nav_path = game::navigate_path(&game_data.p1, game_data.area, &game_data.map.0, game_data.seed);
                game_data.map.0 = nav_path.0;
                game_data.area = nav_path.1;
            },
            | 2 => {
                let nav_path = game::navigate_path(&game_data.p2, game_data.area, &game_data.map.1, game_data.seed);
                game_data.map.1 = nav_path.0;
                game_data.area = nav_path.1;
            },
            | 3 => {
                let nav_path = game::navigate_path(&game_data.p3, game_data.area, &game_data.map.2, game_data.seed);
                game_data.map.2 = nav_path.0;
                game_data.area = nav_path.1;
            },
            | _ => {}
        }
        if !game_data.game_state {
            break;
        } else {
            // game_data.game_state = false;
        }
    }
}

fn format_world(world: String) -> String {
    let world_split: Vec<&str> = world.split(' ').collect();
    world_split.last().unwrap().to_string()
}
// memes
