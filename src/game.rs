use std::io;

pub fn start_game(seed: u64, map1: Vec<u8>, map2: Vec<u8>, map3: Vec<u8>) -> GameData {
    // Prompt user for the class
    let mut choice = String::new();
    println!("What class do you want? 1) Warrior, 2) Wizard, 3) Bard, 4) Rogue");
    io::stdin().read_line(&mut choice).expect("Failed to read line");

    let class: u8;
    loop {
        match &choice[..] {
            | "1\n" => {
                class = 1;
                break;
            },
            | "2\n" => {
                class = 2;
                break;
            },
            | "3\n" => {
                class = 3;
                break;
            },
            | "4\n" => {
                class = 4;
                break;
            },
            | _ => {
                // Check if you insert the right thing this time.
                println!("What class do you want? 1) Warrior, 2) Wizard, 3) Bard, 4) Rogue (This has to be a number from 1-4)");
                // Reset choice because for some reason it just gets added otherwise
                choice = String::new();
                io::stdin().read_line(&mut choice).expect("Failed to read line");
            }
        }
    }
    let mut player = GameData { class,
                                name: String::new(),
                                seed,
                                p1: map1,
                                p2: map2,
                                p3: map3,
                                path: 0,
                                area: 0,
                                game_state: true,
                                map: (MapLog::PathOne(Vec::new()), MapLog::PathTwo(Vec::new()), MapLog::PathThree(Vec::new())) };

    // Get name for some reason (edited)
    println!("What is your name?");
    io::stdin().read_line(&mut choice).expect("Failed to read line");
    player.name = choice;

    // Print the backstory and setup.
    println!(
             "\n\
        A group of goblins invade the town.\n\
        You run to hide and they don't notice you. As you hide, you hear sounds of destruction.\n\
        You leave your hiding spot and see an old man crawl out from the rubble, he seems weary.\n\
        He approaches you. The old man talks to you: \"There is a castle down that road, I've seen it with my own\n\
        eyes, full of goblins, catapults, and other siege weapons. You must go there, kill the general and burn the\n\
        catapults if you wish to save the kingdom.\""
    );
    let weapon = match player.class {
        | 1 => "Wooden Club",
        | 2 => "Earth Staff",
        | 3 => "Spruce Lute",
        | 4 => "Iron Dagger",
        | _ => "punch to the face"
    };

    // Give weapon
    println!("\nThe old man gives you a {}, sending you on your quest.", weapon);

    // Tell location of one town
    println!(
             "\n\
    Before you leave, the old man talks to you again: \"There is a town on the left path after the fork, it's \n\
    close by and they will help you if you're hurt.\"\n\
    Then he leaves, and you embark on your journey.\n"
    );

    // Start the journey
    println!(
             "You walk down the given path and the journey is uneventful up until a fork in the road. You remember the old\n\
        man saying there was a town somewhere to the left.\n\
        Do you wish to go 1) To the left, 2) Straight ahead, or 3) To the right?"
    );
    let mut choice = String::new();
    io::stdin().read_line(&mut choice).expect("Failed to read line");

    player = fork_in_the_road(choice, player, seed);
    player
}

// pub fn resume_game() {}

fn fork_in_the_road(mut choice: String, mut world: GameData, seed: u64) -> GameData {
    let choice_as_u8: u8;
    loop {
        match &choice[..] {
            | "1\n" => {
                choice_as_u8 = 1;
                break;
            },
            | "2\n" => {
                choice_as_u8 = 2;
                break;
            },
            | "3\n" => {
                choice_as_u8 = 3;
                break;
            },
            | _ => {
                // Check if you insert the right thing this time.
                println!("Do you wish to go 1) To the left, 2) Straight ahead, or 3) To the right? (This has to be a number from 1-3)");
                // Reset choice because for some reason it just gets added to otherwise
                choice = String::new();
                io::stdin().read_line(&mut choice).expect("Failed to read line");
            }
        }
    }
    // Add a new line to remove clutter...
    println!("");

    // Set the current path and area.
    match choice_as_u8 {
        | 1 => {
            let mut map = world.p3.clone();
            for i in 0 .. world.p3.len() {
                map[i] = 0;
            }
            let mov = MapLog::PathOne(map);
            let nav_path = navigate_path(&world.p1, 1, &mov, seed);
            world.map.0 = nav_path.0;
            world.area = nav_path.1;
            world.path = 1;
        },
        | 2 => {
            let mut map = world.p3.clone();
            for i in 0 .. world.p3.len() {
                map[i] = 0;
            }
            let mov = MapLog::PathTwo(map);
            let nav_path = navigate_path(&world.p2, 1, &mov, seed);
            world.map.1 = nav_path.0;
            world.area = nav_path.1;
            world.path = 2;
        },
        | 3 => {
            let mut map = world.p3.clone();
            for i in 0 .. world.p3.len() {
                map[i] = 0;
            }
            let mov = MapLog::PathThree(map);
            let nav_path = navigate_path(&world.p3, 1, &mov, seed);
            world.map.2 = nav_path.0;
            world.area = nav_path.1;
            world.path = 3;
        },
        | _ => ()
    };
    // Return updated world with the current area and path.
    world
}

pub fn navigate_path(path: &Vec<u8>, mut area: u8, mov: &MapLog, seed: u64) -> (MapLog, u8) {
    // Set the message for entering a new area
    let mut mov = mov.clone();
    if area as usize >= path.len() {
        println!("You approach a cliff: it's too steep to go down and you turn back.\r\n");
        area -= 2;
    } else {
        let biome = match path[(area - 1) as usize] {
            // 0 is a forest, 1 is a town, 2 is hills, 3 is an outpost, 4 is a desert, 5 is a swamp,
            //   and 6 is a hideout.
            | 0 => "You enter a beautiful forest which eerily fades into darkness as the shadows of the trees cover the ground. It\n\rfeels strangely calm yet threatening, what do you do?",
            | 1 => "You enter a town, to the left you see a sort of hospital, up ahead is a tavern, and to the right are houses and a\n\rshop; the people feel welcoming, what do you do?",
            | 2 => "You walk into a plain of hills and you begin to feel uneasy; you never know whats around the next hill but the\n\rgreen of the plants feels calming and it confuses you; what do you do?",
            | 3 => "You enter an outpost and watch from afar as enemies enter and leave a large stone building; they are armed to the\n\rteeth. What do you do?",
            | 4 => "You enter a barren wasteland, only sand covers the ground as far as you can see and it's hot to the touch, what\n\rdo you do?",
            | 5 => "You enter a swamp and water goes up to your waist. The murky water hides any predators from view and you feel \n\rscared. What do you do?",
            | 6 => "You enter a beautiful forest which eerily fades into darkness as the shadows of the trees cover the ground. It \n\rfeels strangely calm yet threatening, what do you do?",
            | 7 => "You find yourself at the castle walls, what do you do?",
            | _ => ""
        };

        println!("{}", biome);
        // Just make sure they use the right action.
        loop {
            let action = actions(0);
            match &action[..] {
                | "1\n" => {
                    look_around(path[(area - 1) as usize]);
                },
                | "2\n" => {
                    mov = match mov {
                        | MapLog::PathOne(a) => {
                            encounter_enemy(area - 1, a[area as usize - 1], seed, path[area as usize - 1], 1);
                            let mut ennum = a.clone();
                            if a[area as usize - 1] != 4 {
                                ennum[area as usize - 1] += 1;
                                MapLog::PathOne(ennum.clone())
                            } else {
                                MapLog::PathOne(ennum.clone())
                            }
                        },
                        | MapLog::PathTwo(a) => {
                            encounter_enemy(area - 1, a[area as usize - 1], seed, path[area as usize - 1], 2);
                            let mut ennum = a.clone();
                            if a[area as usize - 1] != 4 {
                                ennum[area as usize - 1] += 1;
                                MapLog::PathTwo(ennum.clone())
                            } else {
                                MapLog::PathTwo(ennum.clone())
                            }
                        },
                        | MapLog::PathThree(a) => {
                            encounter_enemy(area - 1, a[area as usize - 1], seed, path[area as usize - 1], 3);
                            let mut ennum = a.clone();
                            if a[area as usize - 1] != 4 {
                                ennum[area as usize - 1] += 1;
                                MapLog::PathThree(ennum.clone())
                            } else {
                                MapLog::PathThree(ennum.clone())
                            }
                        }
                    };
                    break;
                },
                | "3\n" => {
                    if area != 1 {
                        println!("You turn back and leave.");
                        area -= 2;
                        break;
                    } else {
                        println!("You musn't turn back now.");
                    }
                },
                | _ => {}
            }
        }
    }
    (mov.clone(), area)
}

fn actions(situation: u8) -> String {
    match situation {
        // 0 is exploration
        | 0 => println!("1) Look around, 2) Cross the landscape, 3) Leave"),
        | _ => {}
    }
    let mut response = String::new();
    io::stdin().read_line(&mut response).expect("Failed to read line");
    loop {
        match &response[..] {
            | "1\n" => {
                break;
            },
            | "2\n" => {
                break;
            },
            | "3\n" => {
                break;
            },
            | _ => {
                // Check if you insert the right thing this time.
                match situation {
                    | 0 => println!("1) Look around, 2) Cross the landscape, 3) Leave"),
                    | _ => {}
                }
                // Reset choice because for some reason it just gets added to otherwise
                response = "".to_string();
                io::stdin().read_line(&mut response).expect("Failed to read line");
            }
        }
    }
    println!("");
    response
}

fn look_around(biome: u8) {
    match biome {
        // 0 is a forest, 1 is a town, 2 is hills, 3 is an outpost, 4 is a desert, 5 is a swamp,
        //   and 6 is a hideout.
        | 0 => {
            print!("You look around, the trees are far taller than you and the father the forest goes, the less light hits the\n\r\
                      ground. You hear sounds of movement but you can't identify anything. There seems to be foot prints in the mud\n\r\
                      and you see arrows sticking out of the ground just up ahead.");
        },
        | 1 => {
            print!("The townsfolk seem friendly. The pavement on the ground forms a diamond pattern and it looks like it's\n\r\
                      made out of stone. The buildings are made out of wood or stone painted white with limestone. The town must\r\n\
                      be rich.");
        },
        | 2 => {
            print!("The hills are very steep, almost mountainous and you can see birds flying overhead. There is no where to hide\n\r\
                      should any enemies find you atop one of the hills. You see reminents of a camp inbetween some of the hills.");
        },
        | 3 => {
            print!("You can see the base is made out of placed rocks with some sort of limestone mix holding them togethor. The\n\r\
                      middle of the outpost is made from wood, it seems to be weakened and cheap. At the top you see archers in\n\r\
                      steal chainmail and with wooden bows. You see a body hung from the top with a sign but you can't make out the\n\r\
                      words.");
        },
        | 4 => {
            print!("As you glance among the landscape you see only yellows and faint greens. There are small shrubs in between\n\r\
                      the sand. You can easily be spotted from the sand dunes and you begin to feel hot; no water is in sight. On\n\r\
                      one of the dunes you can see the sand painted red by a body; you feel uneasy.");
        },
        | 5 => {
            print!("The water is murky and the ground is littered with mud. The trees here cast dark shadows as they lay in the\n\r\
                      murky waters. You see ripples in the water, and hear the sounds of snakes and gators. There are bloody\n\r\
                      remains of some sort of men.")
        },
        | 6 => {
            print!("You look around, the trees are far taller than you and the father the forest goes, the less light hits the\n\r\
                      ground. You hear sounds of movement but you can't identify anything. There seems to be foot prints in the mud\n\r\
                      and you see arrows sticking out of the ground just up ahead.");
        },
        | _ => {
            panic!("This area doesn't exist or it doesn't support looking around.");
        }
    }
    println!("\nWhat do you do?");
}

fn encounter_enemy(biome: u8, len: u8, seed: u64, mut area: u8, path: u8) {
    // Plan for now: the farther along the path the harder the enemies and better the loot. Seed
    // determines the drop and there are 4 drops on each possible area and after 4 battles you have
    // them all.
    if area != 0 {
        area -= 1;
    }
    let enemy_list: [&str; 96] = [// Forest
                                  "Ratt",
                                  "Wolf",
                                  "Wolf",
                                  "Owl",
                                  "Warg",
                                  "Eagle",
                                  "Bear",
                                  "Giant Owl",
                                  "Bandit",
                                  "Archer",
                                  "Warg Rider",
                                  "Soldier",
                                  "Soldiers",
                                  "Doleney",
                                  "Spearmen",
                                  "Klorx",
                                  // Hills
                                  "Rat",
                                  "Weeds",
                                  "Bobcat",
                                  "Sparrow",
                                  "Zombie",
                                  "Owl",
                                  "Weeping Willow",
                                  "Goblin",
                                  "Adventurer",
                                  "Undead",
                                  "Undead",
                                  "Troll",
                                  "Undead",
                                  "Ogre",
                                  "Ogre",
                                  "Hill Bard",
                                  // Outpost
                                  "Scout",
                                  "Mouse",
                                  "Soldier",
                                  "Crossbowman",
                                  "Soldier",
                                  "Swordsman",
                                  "Spearmen",
                                  "Archer",
                                  "Yentu",
                                  "Soldiers",
                                  "Kralkon",
                                  "Yentu",
                                  "Kralkons",
                                  "Yentus",
                                  "Cafras",
                                  "Srew",
                                  // Desert
                                  "Scorpians",
                                  "Fox",
                                  "Begingo",
                                  "Scout",
                                  "Camel",
                                  "Eraldun",
                                  "Eraldun",
                                  "Begingo",
                                  "Soldier",
                                  "Eraldun",
                                  "Est",
                                  "Soldier",
                                  "Est",
                                  "Adventurer",
                                  "Sand Dragon",
                                  "Sand Mage",
                                  // Swamp
                                  "Piranha",
                                  "Snapping Turtle",
                                  "Barracuda",
                                  "Piranha",
                                  "Snake",
                                  "Tyosil",
                                  "Tyosil",
                                  "Eel",
                                  "Octopus",
                                  "Soldier",
                                  "Crocodile",
                                  "White Snake",
                                  "Yentu",
                                  "Crocodile",
                                  "Crocodile",
                                  "Yentu",
                                  // Hideout
                                  "Bandit",
                                  "Scout",
                                  "Soldier",
                                  "Archer",
                                  "Warg",
                                  "Soldier",
                                  "Soldiers",
                                  "Spearmen",
                                  "Soldiers",
                                  "Bear",
                                  "Adventurer",
                                  "Executer",
                                  "Executer",
                                  "Yentu",
                                  "Executer",
                                  "Rogue"];
    let area = match area >= 9 {
        | true => 8,
        | false => area
    };
    // After fighting for enemies the area is safe to travel through
    if len <= 3 {
        // Len can't be more than three, but rust thinks it's a u64...
        let len = match u64::pow(seed * biome as u64, path as u32) % 4 {
            | 0 => match len {
                | 0 => 0,
                | 1 => 0,
                | 2 => 1,
                | _ => 1
            },
            | 1 => match len {
                | 0 => 1,
                | 1 => 1,
                | 2 => 0,
                | _ => 0
            },
            | 2 => match len {
                | 0 => 0,
                | 1 => 1,
                | 2 => 0,
                | _ => 1
            },
            | _ => match len {
                | 0 => 1,
                | 1 => 0,
                | 2 => 1,
                | _ => 0
            }
        };
        let len = ((biome) * 2) + len;
        println!("You encounter a {}!", enemy_list[((area * 16) + len) as usize]);
    }
}

pub struct GameData {
    // Class is the character's class, like rogue or wizard
    pub class:      u8,
    // Character name
    pub name:       String,
    // Worldseed
    pub seed:       u64,
    // Path1's areas
    pub p1:         Vec<u8>,
    // Path2's areas
    pub p2:         Vec<u8>,
    // Path3's areas
    pub p3:         Vec<u8>,
    // Current path
    pub path:       u8,
    // Current area
    pub area:       u8,
    // Record of current areas travelled and how much
    pub map:        (MapLog, MapLog, MapLog),
    // Whether it's running or not
    pub game_state: bool
}

#[derive(Clone)]
pub enum MapLog {
    PathOne(Vec<u8>),
    PathTwo(Vec<u8>),
    PathThree(Vec<u8>)
}
